# tela-icon-theme

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

A flat colorful Design icon theme

https://github.com/vinceliuice/Tela-icon-theme

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/icons-and-themes/tela-icon-theme.git
```
